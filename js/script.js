$.ajax({
   method: 'GET',
   url: 'https://restcountries.eu/rest/v2/all?fields=name',
   success: response => {
      const options = {
         data: response,
         getValue: "name",
         list: {
            match: {
               enabled: true
            }
         },
         theme: "square"
      };
      $('#f-country').easyAutocomplete(options);
   }
});